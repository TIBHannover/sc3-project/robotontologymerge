# RobotOntologyMerge



## Digital Reference Ontology Continuous Integration, Merging Pipeline

For the merging operation we have choosen Robot which is an open source tool most popular among the ontolgoy development community. Robot tool provides many different operations on ontolgoy. In this section, we provide a description of the continuous integration pipeline for the merging of two ontologies using Robot tool. SC3 provides a pipeline (below Figure), which takes three variables as input, the first two inputs each takes ontology files as input and the 3rd variable is for providing a commit message to push the merge output back into git (GitLab repository).

## How to run pipeline


<kbd>![Alt text](<image.png>)

The pipeline goes through the following three stages as shown in Figure 2:
1.	**Build**: In this stage Robot source is cloned and build from its publically available repository on 	GitHub.
2.	**Merge**: In this stage, the two ontologies is retrieved from Gitlab and is merged using merge 	command of robot ontology tool
3.	**Push**: In this stage, a new branch is created in GitLab from the master branch and the results of the merge is pushed into this new branch.

## Pipleline stages


<kbd>![Alt text](image-1.png)</kbd>

We have tested the robot merge pipline with Demand Fulfillment Ontology merged into Digital Reference. The results are available in the SC3 GitLab repository under Digital Reference Fork git repository. 